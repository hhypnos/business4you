<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.*','mainpage.*'],  function(View $view){
            $view->with('languages', \App\Language::get());
            $view->with('currencies', \App\Currency::get());

            $view->with('static_word', function($page,$key){
                return translate(\App\Static_word::where('page',$page)->where('key',$key)->first(),
                session("languageID"));
            });

            $view->with('static_image', function($page,$key){
                return \App\Static_image::where('page',$page)->where('key',$key)->first();
            });

            $view->with('cover_image', function(){
                $cover_image = \App\Cover_image::get()->toArray();
                $random_key = mt_rand(0,count($cover_image)-1);
                return $cover_image[$random_key];
            });
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
