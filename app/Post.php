<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function scopeChooseLanguage($query){
       return $query->where(function ($query) {
            $query->where('langID',session('languageID'))
                ->orWhere('langID', 1);
        });
    }
    public function cover_image(){
        return $this->hasOne('App\Published_image','contentID','posts_id')->where('image_genderID',1)->where('defaultOrNot','2')->orderBy('id','desc');
    }

    public function all_images(){
        return $this->hasMany('App\Published_image','contentID','posts_id')->where('image_genderID',1)->orderBy('order_id','asc');
    }

    public function post_texts(){
        return $this->hasMany('App\Published_text','contentID','posts_id')->orderBy('id','asc');
    }
}
