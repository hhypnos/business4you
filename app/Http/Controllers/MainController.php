<?php

namespace App\Http\Controllers;

use App\Language;
use App\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    private $language;
    public function __construct()
    {
        if(session('languageID') === null){
            session(['languageID'=>1]);
        }
        $this->language = session('languageID');
    }


    /**    public function index(){
    $posts = Post::orderby('created_at','desc')->get();

    return view('mainpage.welcome',compact('posts'))->with([
    "title"=>translate('business 4 you',session('languageID')),
    "description"=>translate('support 4 business',session('languageID'))
    ]);
    }
     *
     * Main Controller
     *
     **/
    public function index(){
        $posts = Post::chooseLanguage()->where('post_typeID',1)->orderby('langID','desc')->orderby('order_id','asc')->get()->unique('posts_id');


        return view('mainpage.welcome',compact('posts'))->with([
            "title"=>translate('business 4 you',session('languageID')),
            "description"=>translate('support 4 business',session('languageID'))
        ]);
    }

    public function post($slug){
        $post = Post::where('slug', $slug)->where('langID', session('languageID'))->first();
        if(!Post::where('slug',$slug)->where('langID',session('languageID'))->exists()) {
            $post = Post::where('slug', $slug)->where('langID', 1)->firstOrFail();
        }
        return view('mainpage.post',compact('post'))->with(["title"=>$post->title,"description"=>$post->metaDescription]);
    }




    /**
     * Send feedback
     **/
    public function send_feedback()
    {
        $this->validate(request(),[
            'email'=>'required|email',
            'text'=>'required',
            'g-recaptcha-response' => 'required'
        ]);
        $emailOptions = array(
            'email'=>request('email'),
            'text' => request('text')
        );
        $g_recaptcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfQUq4UAAAAANYGLkaANyc5vHyWA8YX9bA6V-zH&response=".request('g-recaptcha-response')."&remoteip=".request()->ip()), true);

        if(!$g_recaptcha['success']){
            return response()->JSON(['info'=>'error on human check']);
        }
        \Mail::send([], $emailOptions, function($message){
            $message->to('world4you9.travel@gmail.com')->subject('Business4you Feedback:');
            $message->from(request('email') ,request('email'));
            $message->setBody(request('text') );
        });

        session(['mailsent'=>'ok']);
        return redirect('/');
    }


    /**
     * @param $lang
     * @return translated text
     **/
    public function language($lang)
    {
        $language = Language::where('langID',$lang)->first();

        if($language){
            session(['languageID' => $language->langID]);
            if (url()->previous() != url()->current() && strpos(url()->previous(), env('APP_URL')) !== false) {
                return redirect(url()->previous());

            }
            return redirect(url()->to('/'));
        }
        return redirect('/')->withErrors(['error'=>translate('language not found',session('languageID'))]);
    }

}
