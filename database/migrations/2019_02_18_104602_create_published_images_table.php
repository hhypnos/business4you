<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublishedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('published_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imageID');
            $table->integer('image_genderID');
            $table->integer('contentID');
            $table->integer('order_id')->nullable();
            $table->enum('defaultOrNot',['1','2'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('published_images');
    }
}
