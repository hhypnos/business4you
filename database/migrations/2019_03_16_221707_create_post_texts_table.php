<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_texts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('post_texts_id')->nullable();
            $table->integer('userID')->nullable();
            $table->string('title')->nullable();
            $table->longText('text');
            $table->enum('leftOrRight',['right','left'])->default('right');
            $table->integer('breadcrumbs')->default(1);
            $table->integer('langID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_texts');
    }
}
