<div class="main-advantages container" style="display:block;margin-top:115px;">
    <div class="row">
        <div class="col">
            <h5 class="text-dark">{{translate('Planning business here?',session('languageID'))}}</h5>
            <p style="font-size:15px;">{{translate('We offer a range of services to start and support your business in Georgia. If your requirement is not in our list of services, do not hesitate to contact us',session('languageID'))}}</p>
            <hr>
            <div class="col basic-services btn btn-light" data-toggle="modal" data-target="#dataModal" onclick="fetchData('{{URL::to('business-research')}}')">
                {{translate('Business research & Support',session('languageID'))}}
            </div>
            <div class="col basic-services btn btn-light" data-toggle="modal" data-target="#dataModal" onclick="fetchData('{{URL::to('legal-support')}}')">
                {{translate('Legal support & Business licenses',session('languageID'))}}
            </div>
            <div class="col basic-services btn btn-light" data-toggle="modal" data-target="#dataModal" onclick="fetchData('{{URL::to('accounting-advisory')}}')">
                {{translate('Accounting service & Audit',session('languageID'))}}
            </div>
            <div class="col basic-services btn btn-light" data-toggle="modal" data-target="#dataModal" onclick="fetchData('{{URL::to('real-estate')}}')">
                {{translate('Real Estate & Accommodation',session('languageID'))}}
            </div>
        </div>
        <div class="col">

            @if(is_null(session('mailsent')))
                <form method="post" action="{{route('sendfeedback')}}">
                    {{csrf_field()}}
                    <h5 class="text-dark">{{translate('Briefly inform us of your plans in Georgia.!',session('languageID'))}}</h5>
                    <p style="font-size:15px;">{{translate("We'll make needed arrangements for your enjoyment and success! Just shoot your request or question. Our online advice is free",session('languageID'))}}</p>
                    <hr style="margin-top: -3px;">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" aria-describedby="name" placeholder="{{translate('Your name',session('languageID'))}}">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" aria-describedby="email" placeholder="{{translate('Your E-mail',session('languageID'))}}">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" rows="3" name="text" placeholder="{{translate('Your Message',session('languageID'))}}"></textarea>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LfQUq4UAAAAAD91TyUAxbiG5Sc9D33mQm4kSvb2"></div>
                    <button type="submit" class="btn btn-success" style="backgrund:#5ac0ed;position: absolute;top: 317px;right: 11px;height: 76px;width: 38%;">{{translate('Send',session('languageID'))}}</button>
                </form>
            @else
                @php(session()->forget('mailsent'))
                <h1 style="text-align: center">{{translate('Mail sent',session('languageID'))}}</h1>
            @endif

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 1139px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="model-content-body">
                <img src="{{URL::to('assets/images/preloader.svg')}}"  style='width:250px;position: relative;left: 40%;'>
            </div>

        </div>
    </div>
</div>

