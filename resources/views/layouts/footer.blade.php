<footer class="footer" style="background:#444444">
    {{--<div class="container bottom_border">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-4 col-md col-sm-4 col-12 col">--}}
    {{--<h5 class="headin5_amrc col_white_amrc pt2">About Us</h5><!--headin5_amrc-->
    {{--<p class="mb10">Business 4 You is a business support portal with useful information and advices for prospective investors in Georgia.--}}
    {{--Best info page about the business climat and safety in this country, local culture, habits and cuisine.--}}

    {{--Here you will find our services in:--}}
    {{--Market research, registration, licenses and professional staff.--}}
    {{--Accounting and finance management.--}}
    {{--Legal support, representation in the court, assistance in commercial disputes.--}}
    {{--The website is constantly updated and reviewed. We are always working on adding more and more interesting stuff. Enjoy browsing the site and feel free to contact us - we are working for your convenience !</p>--}}
    {{--<p><i class="fa fa-location-arrow"></i> 24a Shalva Dadiani str.</p>--}}
    {{--<p><i class="fa fa-phone"></i> +557 17 11 44</p>--}}
    {{--<p><i class="fa fa fa-envelope"></i> info@world4you.travel</p>--}}
    {{--</div>--}}
    {{--<div class=" col-sm-4 col-md col-6 col">--}}
    {{--<h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5><!--headin5_amrc-->--}}
    {{--<ul class="footer_ul_amrc">--}}
    {{--<li>--}}
    {{--<a href="{{URL::to('/')}}">Home</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">About Us</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">F.A.Q</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Contact Us</a>--}}
    {{--</li>--}}

    {{--</ul><!--footer_ul_amrc ends here-->--}}
    {{--</div>--}}
    {{--<div class=" col-sm-4 col-md col-6 col">--}}
    {{--<h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5><!--headin5_amrc-->--}}
    {{--<ul class="footer_ul_amrc">--}}
    {{--<li>--}}
    {{--<a href="#">Remove Background</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Shadows & Mirror Reflection</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Logo Design</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Vectorization</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Hair Masking/Clipping</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">Image Cropping</a>--}}
    {{--</li>--}}
    {{--</ul><!--footer_ul_amrc ends here-->--}}
    {{--</div>--}}
    {{--<div class=" col-sm-4 col-md col-12 col">--}}
    {{--<h5 class="headin5_amrc col_white_amrc pt2">Follow us</h5><!--headin5_amrc ends here-->--}}
    {{--<ul class="footer_ul2_amrc">--}}
    {{--<li>--}}
    {{--<a href="https://www.facebook.com/cars4rentge/"><i class="fab fa-facebook fleft padding-right"></i></a>--}}
    {{--<p><a href="https://cars4rent.ge">CARS 4 RENT | Cheapest Car Rental with Best Service in Georgia</a></p>--}}
    {{--</li>--}}

    {{--<li>--}}
    {{--<a href="https://www.facebook.com/georgia4you.ge/"><i class="fab fa-facebook fleft padding-right"></i></a>--}}
    {{--<p><a href="https://georgia4you.ge">GEORGIA 4 YOU | Best info site for travelers to Georgia</a></p>--}}
    {{--</li>--}}
    {{--</ul><!--footer_ul2_amrc ends here-->--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="container">
        <!--<ul class="foote_bottom_ul_amrc">-->
        <!--	<li>-->
        <!--		<a href="#">Home</a>-->
        <!--	</li>-->

        <!--	<li>-->
        <!--		<a href="#">F.A.Q.</a>-->
        <!--	</li>-->
        <!--	<li>-->
        <!--		<a href="#">Contact us</a>-->
        <!--	</li>-->
        <!--</ul>foote_bottom_ul_amrc ends here-->
        <h3 style="color: white;padding-top:10px;line-height:3px;">{{translate('About Us',session('languageID'))}}</h3>
        <br/>
        <p style="font-size:14px;">{{translate('Business 4 You is a business support portal with useful information and advices for prospective investors in Georgia.
Best info page about the business climat and safety in this country, local culture, habits and cuisine.

Here you will find our services in:
Market research, registration, licenses and professional staff.
Accounting and finance management.
Legal support, representation in the court, assistance in commercial disputes.
			The website is constantly updated and reviewed. We are always working on adding more and more interesting stuff. Enjoy browsing the site and feel free to contact us - we are working for your convenience !',session('languageID'))}}</p>
        <p class="text-center">{!!translate('Copyright &copy; 2019 | Created by',session('languageID'))!!}<a href="#">4 Services Group</a></p>

    </div>
</footer>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/3ac4efbf68.js"></script>


<script>
    let fetchData = (url) => {

        $( '#model-content-body' ).html("<img src='{{URL::to('assets/images/preloader.svg')}}' style='width:250px;position: relative;left: 40%;'>");
        $( '#dataModalLabel').html('');
        $( '#dataModalLabel' ).load(url + " "+'.left-floated h1' );

        $( '#model-content-body' ).load(url + " "+'.left-floated',()=>{
            $('.left-floated h1').remove();

        } );

    }

</script>
