<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="{{env('APP_URL')}}" />
    @foreach($languages as $language)
        <link rel="alternate" hreflang="{{$language->langAcronym}}" href="{{URL::to('language')}}/{{$language->langID}}" />
    @endforeach
    {{--<meta property="fb:app_id" content="1940773326029040"/>--}}
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta property="og:url" content="{{Request::segment(2) ? Request::url() : env('APP_URL')}}" />
    <meta property="og:type" content="{{Request::segment(2) ? 'article' : 'website'}}" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:description" content="{{$description}}" />
    <meta property="og:image" content="{{Request::segment(2) ? URL::to($post->cover_image['image']['image']) : URL::to('icons/favicon-512x512.png')}}" />

    {{--<link rel="apple-touch-icon" sizes="180x180" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="32x32" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="#">--}}
    {{--<link rel="shortcut icon" type="image/ico" href="{{URL::to('assets/images/fav.ico')}}">--}}
    {{--<link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('icons/apple-touch-icon.png')}}">--}}
    {{--<link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('icons/favicon-32x32.png')}}">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('icons/favicon-16x16.png')}}">--}}
    {{--<link rel="manifest" href="{{URL::to('icons/site.webmanifest')}}">--}}
    {{--<link rel="mask-icon" href="{{URL::to('icons/safari-pinned-tab.svg')}}" color="#5bbad5">--}}
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('assets/images/fav.ico')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('assets/images/fav.ico')}}">
    <link rel="shortcut icon" href="{{URL::to('assets/images/fav.ico')}}">
    <meta name="msapplication-TileColor" content="#a3ce54">
    <meta name="theme-color" content="#ffffff">
    {{--<link rel="mask-icon" href="" color="#a3ce54">--}}
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#a3ce54">
    <meta name="description" content="{{$description}}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{$title}}" />
    <script type="application/ld+json">
        {
          "@context": "https:\/\/schema.org",
          "@type": "WebSite",
          "url": "{{url()->full()}}",
          "name": "{{$title}}",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+(995)-557-17-11-44",
            "contactType": "Customer service"
          },
          "logo":"{{URL::to('assets/images/fav.ico')}}",
          {{--"sameAs":["https:\/\/www.facebook.com\/georgia4you.ge"]--}}

        }
</script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::to('assets/css/style.css')}}">
    <title>{{$title}}</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>
<body>
<header>
    <div class="top-bar">
        <div class="inner-top-bar">
            <div class="alt-service-logos">


                <a href="http://georgia4you.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img alt="GEORGIA 4 YOU LOGO" src="{{URL::to('assets/images/services-logo-georgia4you.svg')}}">
                        <div class="logo-label">
                            <div class="arrow-up"></div><span class="logo-label-title">GEORGIA 4 YOU</span>
                        </div>
                    </div>
                </a>
                <a href="http://cars4rent.ge">
                    <div class="alt-service-logos-single-logo-containter alt-service-logos-single-logo-containter-active">
                        <img alt="CARS 4 RENT LOGO" src="{{URL::to('assets/images/services-logo-cars4rent.svg')}}">
                        <div class="logo-label logo-label-active">
                            <div class="arrow-up"></div><span class="logo-label-title">CARS 4 RENT</span>
                        </div>
                    </div>
                </a>
                <a href="http://rooms4rent.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img alt="ROOMS 4 RENT LOGO" src="{{URL::to('assets/images/services-logo-rooms4rent.svg')}}">
                        <div class="logo-label">
                            <div class="arrow-up"></div><span class="logo-label-title">ROOMS 4 RENT</span>
                        </div>
                    </div>
                </a>
                <a href="http://business4you.ge" target="_blank">
                    <div class="alt-service-logos-single-logo-containter">
                        <img alt="SUPPORO 4 BUSINESS LOGO" src="{{URL::to('assets/images/services-logo-support4biz.svg')}}">
                        <div class="logo-label">
                            <div class="arrow-up"></div><span class="logo-label-title">SUPPORT 4 BUSINESS</span>
                        </div>
                    </div>
                </a>


                <div class="clear"></div>
            </div><a href="{{URL::to('/')}}"><img alt="BUSINESS 4 YOU LOGO" class="alt-logo" src="{{URL::to('assets/images/logo-inverted.svg')}}"> <img alt="CARS 4 RENT LOGO" class="alt-small-logo" src="{{URL::to('assets/images/services-logo-cars4rent.svg')}}"></a>
            <div class="top-bar-right-menu">

                <div class="language-chooser">

                    @foreach($languages as $language)
                        @if($language->langID == session('languageID'))
                            <img src="{{$language->langImage}}" alt="language switch {{$language->language}}" style="width:22px;position:relative;top:-1px;" />
                            <span>{{$language->language}}</span>
                        @endif
                    @endforeach

                    <div class="language-chooser-dropdown">
                        @foreach($languages as $language)
                            <a href="{{URL::to('language')}}/{{$language->langID}}" style="text-transform:uppercase">
                                <img src="{{URL::to($language->langImage)}}" alt="language switch {{$language->language}}" style="width:22px;position:relative;top:-1px;" />
                                {{$language->language}}
                            </a>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div><a href="{{URL::to('/')}}"><img alt="CARS 4 RENT LOGO" class="logo" src="{{URL::to('assets/images/business4you.svg')}}"></a>
    <!--<nav>-->
<!--    <a class="active-nav-button" href="{{URL::to('/')}}">Home-->
<!--        <div class="short-border"></div></a> <a href="{{URL::to('/')}}">Contact us-->
    <!--        <div class="short-border"></div></a>-->
    <!--</nav>-->
    <div class="slogan" style="font-size:50px;    color: #5ac0ed;">
        {{translate('Our Skill Is Working For You !',session('languageID'))}}
    </div>
    <div class="services">
        <div class="inner-services">
            <a href="http://georgia4you.ge" target="_blank">
                <div class="service-logos services-logo-georgia4you">
                    <div class="thick-border-left"></div>
                    <div class="service-logo-button">
                        <img alt="GEORGIA 4 YOU LOGO" src="{{URL::to('assets/images/services-logo-georgia4you.svg')}}">
                        <div class="service-logo-button-text">
                            GEORGIA 4 YOU
                        </div>
                    </div>
                    <div class="thick-border-right"></div>
                </div>
            </a>
            <a href="http://cars4rent.ge" target="_blank">
                <div class="service-logos services-logo-cars4rent">
                    <div class="thick-border-left"></div>
                    <div class="service-logo-button">
                        <img alt="CARS 4 RENT LOGO" src="{{URL::to('assets/images/services-logo-cars4rent.svg')}}">
                        <div class="service-logo-button-text">
                            CARS 4 RENT
                        </div>
                    </div>
                    <div class="thick-border-right"></div>
                </div>
            </a>
            <a href="http://autos4share.com" target="_blank">
                <div class="service-logos services-logo-rooms4rent">
                    <div class="thick-border-left"></div>
                    <div class="service-logo-button">
                        <img alt="SUPPORT 4 BIZ LOGO" src="{{URL::to('assets/images/services-logo-support4biz.svg')}}">
                        <div class="service-logo-button-text">
                            AUTOS 4 SHARE
                        </div>
                    </div>
                    <div class="thick-border-right"></div>
                </div>
            </a>
            <a href="http://business4you.ge">
                <div class="service-logos services-logo-support4biz services-logo-active">
                    <div class="thick-border-left"></div>
                    <div class="service-logo-button">
                        <img alt="ROOMS 4 RENT LOGO" src="{{URL::to('assets/images/services-logo-rooms4rent.svg')}}">
                        <div class="service-logo-button-text">
                            BUSINESS 4 YOU
                        </div>
                    </div>
                    <div class="thick-border-right"></div>
                </div>
            </a>
            <div class="line-through-services"></div>
        </div>
        <div class="line-through-services"></div>
    </div>
</header>
