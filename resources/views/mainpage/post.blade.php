@extends('layouts.master')
@section('content')
    <main>
        <div class="topic-full-description">
            @php($prev = App\Post::chooseLanguage()->where('id', '<', $post->id)->where('post_typeID',1)->orderBy('id','desc')->first())
            <a href="{{URL::to($prev['slug'])}}"><span style="position: absolute;left: 60px;">{{$prev['title']}}</span><i class="fas fa-arrow-left" style="font-size:24px;color:#9e9e9e"></i></a>

            @php($next = App\Post::chooseLanguage()->where('id', '>', $post->id)->where('post_typeID',1)->first())

            <a href="{{URL::to($next['slug'])}}"><span style="position: absolute;right: 60px;">{{$next['title']}}</span><i class="fas fa-arrow-right" style="font-size:24px;color:#9e9e9e;float:right"></i></a>
            <hr>
            @foreach($post->post_texts as $post_text)
                <div class="{{$post_text->text->leftOrRight}}-floated">
                    <h1>{{$post_text->text->title}}</h1>

                    @if(!$post_text->text->text_images->isEmpty())
                        <img src="{{URL::to($post_text->text->text_images[0]->image_data->image)}}" alt="">
                    @endif
                    {!! $post_text->text->text !!}
                </div>

                <div class="clear"><div>
                        <br/>
                        @endforeach
                    </div>
    </main>

@endsection
