@extends('layouts.master')
@section('content')

    <main>
        @include('layouts.mainadvantages')
        <div class="main-topics container">
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="main-topic-item" style="
              background: url('{{$post->cover_image['image']['image']}}') no-repeat;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
            ">
                        <div class="main-topic-item-title">
                            <h2>{{$post->title}}</h2>
                        </div>
                        <div class="main-topic-item-description">
                            <h2>{{$post->title}}</h2>
                            <p>
                                {{$post->description}}
                                <a href="{{$post->slug}}" >Read More...</a>
                            </p>
                        </div>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </main>

@endsection