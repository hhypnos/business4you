-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 01, 2019 at 09:14 PM
-- Server version: 10.2.12-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trips45_business4you`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryID` int(11) NOT NULL,
  `cityID` int(11) NOT NULL,
  `city` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryID` int(11) NOT NULL,
  `country` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `currencyID` int(11) NOT NULL,
  `currency` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `currencyID`, `currency`, `logo`, `created_at`, `updated_at`) VALUES
(1, 1, 'USD', '$', NULL, NULL),
(2, 2, 'EUR', '&euro;', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `image_thumbnail`, `alt`, `created_at`, `updated_at`) VALUES
(1, 'https://mercure.accorhotels.com/local-stories/media/tbilisi_desk.jpg', 'https://mercure.accorhotels.com/local-stories/media/tbilisi_desk.jpg', 'd', NULL, NULL),
(2, 'assets/images/post_texts/Georgia.jpg', 'assets/images/post_texts/Georgia.jpg', 'Georgia Flag', NULL, NULL),
(3, 'assets/images/post_texts/visas.jpg', 'assets/images/post_texts/visas.jpg', 'Georgia visas', NULL, NULL),
(4, 'assets/images/post_texts/cuisine.jpg', 'assets/images/post_texts/cuisine.jpg', 'Georgia causine', NULL, NULL),
(5, 'assets/images/post_texts/13.png', 'assets/images/post_texts/13.png', 'Georgia culture', NULL, NULL),
(6, 'assets/images/post_texts/w11.jpg', 'assets/images/post_texts/w11.jpg', 'Georgia transport', NULL, NULL),
(7, 'assets/images/post_texts/safety_large.jpg', 'assets/images/post_texts/safety_large.jpg', 'Georgia safety', NULL, NULL),
(8, 'assets/images/post_texts/customs.jpg', 'assets/images/post_texts/customs.jpg', 'Georgia custom', NULL, NULL),
(9, 'assets/images/post_texts/communication.jpg', 'assets/images/post_texts/communication.jpg', 'Georgia communication', NULL, NULL),
(10, 'https://mercure.accorhotels.com/local-stories/media/tbilisi_desk.jpg', 'https://mercure.accorhotels.com/local-stories/media/tbilisi_desk.jpg', 'd', NULL, NULL),
(11, 'assets/images/post_texts/georgia_cover.jpg', 'assets/images/post_texts/georgia_cover.jpg', 'Georgia Flag', NULL, NULL),
(12, 'assets/images/post_texts/visas_cover.jpg', 'assets/images/post_texts/visas_cover.jpg', 'Georgia visas', NULL, NULL),
(13, 'assets/images/post_texts/cuisine_cover.jpg', 'assets/images/post_texts/cuisine_cover.jpg', 'Georgia causine', NULL, NULL),
(14, 'assets/images/post_texts/13_cover.jpg', 'assets/images/post_texts/13_cover.png', 'Georgia culture', NULL, NULL),
(15, 'assets/images/post_texts/w11_cover.jpg', 'assets/images/post_texts/w11_cover.jpg', 'Georgia transport', NULL, NULL),
(16, 'assets/images/post_texts/safety_large_cover.jpg', 'assets/images/post_texts/safety_large_cover.jpg', 'Georgia safety', NULL, NULL),
(17, 'assets/images/post_texts/customs_cover.jpg', 'assets/images/post_texts/customs_cover.jpg', 'Georgia custom', NULL, NULL),
(18, 'assets/images/post_texts/communication_cover.jpg', 'assets/images/post_texts/communication_cover.jpg', 'Georgia communication', NULL, NULL),
(19, 'assets/images/post_texts/business-research.jpg', 'assets/images/post_texts/business-research.jpg', 'business-research', NULL, NULL),
(20, 'assets/images/post_texts/compprofil.jpg', 'assets/images/post_texts/compprofil.jpg', 'business-research', NULL, NULL),
(21, 'assets/images/post_texts/Financial Accounting Advisory Services.jpg', 'assets/images/post_texts/Financial Accounting Advisory Services.jpg', 'business-research', NULL, NULL),
(22, 'assets/images/post_texts/home.jpg', 'assets/images/post_texts/home.jpg', 'business-research', NULL, NULL),
(23, 'assets/images/post_texts/Reasonable-Accommodation.jpg', 'assets/images/post_texts/Reasonable-Accommodation.jpg', 'business-research', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `langID` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langAcronym` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `langID`, `language`, `langAcronym`, `langImage`, `created_at`, `updated_at`) VALUES
(1, 1, 'ENGLISH', 'EN', 'assets/icons/language/english.svg', NULL, NULL),
(2, 2, 'RUSSIAN', 'RU', 'assets/icons/language/russian.svg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_07_15_160724_create_countries_table', 1),
(3, '2018_08_08_004003_create_languages_table', 1),
(4, '2018_08_09_090219_create_cities_table', 1),
(5, '2018_11_08_123829_create_translates_table', 1),
(6, '2018_11_08_211507_create_currencies_table', 1),
(7, '2018_11_25_232604_create_static_words_table', 1),
(8, '2018_11_29_000011_create_static_images_table', 1),
(9, '2019_01_24_110430_create_images_table', 1),
(10, '2019_01_22_130836_create_posts_table', 2),
(11, '2019_02_18_104602_create_published_images_table', 2),
(12, '2019_03_16_221707_create_post_texts_table', 2),
(13, '2019_03_16_222033_create_post_texts_images_table', 2),
(14, '2019_03_16_223335_create_published_texts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `posts_id` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_typeID` int(11) NOT NULL DEFAULT 1,
  `order_id` int(11) DEFAULT NULL,
  `langID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `posts_id`, `userID`, `title`, `subtitle`, `description`, `metaDescription`, `text`, `slug`, `post_typeID`, `order_id`, `langID`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Basic Facts & Figures', 'Basic Facts & Figures', 'Population of Georgia is 4,469,200 , Tbilisi -1,225,000\r\nEthnic composition: Georgians 84%, Azerbaijanis 6.5...', 'metaDescription', 'text', 'basic-facts', 1, 1, 1, NULL, NULL),
(2, 2, NULL, 'Visas', 'Visas', 'You do not need a visa to enter and stay in Georgia for up to 360 days if you...', 'metaDescription', 'text', 'visas', 1, 2, 1, NULL, NULL),
(3, 3, NULL, 'Cuisine', 'Cuisine', 'Georgians are proud of their national cuisine, and rightly so...', 'metaDescription', 'text', 'cuisine', 1, 3, 1, NULL, NULL),
(4, 4, NULL, 'Culture', 'Culture', 'Georgian culture is unique and vibrant. Georgians treat...', 'metaDescription', 'text', 'culture', 1, 4, 1, NULL, NULL),
(5, 5, NULL, 'Arrival & Transport', 'Arrival & Transport', 'Tbilisi International airport is about 17,5 km East of Tbilisi Centre...', 'metaDescription', 'text', 'arrival-transport', 1, 5, 1, NULL, NULL),
(6, 6, NULL, 'Safety', 'Safety', 'You can bring 400 cigarettes, or 50 cigars or 50 cigarillos...Tbilisi is as safe as many European cities. In essence...', 'metaDescription', 'text', 'safety', 1, 6, 1, NULL, NULL),
(7, 7, NULL, 'Customs', 'Customs', 'You can bring 400 cigarettes, or 50 cigars or 50 cigarillos...', 'metaDescription', 'text', 'customs', 1, 7, 1, NULL, NULL),
(8, 8, NULL, 'Communication', 'Communication', 'WiFi network works in some city areas. It is free and doesn’t require any registration, but is rather slow...', 'metaDescription', 'text', 'communication', 1, 8, 1, NULL, NULL),
(9, 9, NULL, 'Business research & Support', 'Business research & Support', 'Accurate information is vital for making successful business and marketing decisions.\r\nWe provide custom-tailored solutions that will meet your research objectives. ', 'metaDescription', 'text', 'business-research', 2, 1, 1, NULL, NULL),
(10, 10, NULL, 'Legal Support Services', 'Legal Support Services', 'We assist non-Georgian businesses introducing themselves into the local market, support the set-up of incorporation / branch in Georgia, can obtain needed business licenses for foreign-owned companies.', 'metaDescription', 'text', 'legal-support', 2, 2, 1, NULL, NULL),
(11, 11, NULL, 'Accounting Advisory Services & Audit', 'Accounting Advisory Services & Audit', 'As private businessmen and companies continue to seek new developing markets, challenges clearly remain.', 'metaDescription', 'text', 'accounting-advisory', 2, 3, 1, NULL, NULL),
(12, 12, NULL, 'Real Estate & Accommodation', 'Real Estate & Accommodation', 'Our company offers you a quality service:\r\n- To find and arrange a sale or lease of property...', 'metaDescription', 'text', 'real-estate', 2, 4, 1, NULL, NULL),
(13, 1, NULL, 'Основные факты и цифры', 'Основные факты и цифры', 'Население Грузии составляет 4 469 200 человек, Тбилиси - 1 225 000\r\nЭтнический состав: грузины - 84%, азербайджанцы - 6,5%...', 'metaDescription', 'text', 'basic-facts', 1, 1, 2, NULL, NULL),
(14, 2, NULL, 'Визы', 'Визы', 'Вам не нужна виза для въезда и пребывания в Грузии на срок до 360 дней, если вы...', 'metaDescription', 'text', 'visas', 1, 2, 2, NULL, NULL),
(15, 3, NULL, 'Кухня', 'Кухня', 'Грузины гордятся своей национальной кухней, и это правильно...', 'metaDescription', 'text', 'cuisine', 1, 3, 2, NULL, NULL),
(16, 4, NULL, 'культура', 'культура', 'Грузинская культура уникальна и ярка. Грузины относятся к своей культуре очень серьезно...', 'metaDescription', 'text', 'culture', 1, 4, 2, NULL, NULL),
(17, 5, NULL, 'Прибытие и транспортировка', 'Прибытие и транспортировка', 'Тбилисский международный аэропорт находится примерно в 17,5 км от центра Тбилиси...', 'metaDescription', 'text', 'arrival-transport', 1, 5, 2, NULL, NULL),
(18, 6, NULL, 'Безопасности', 'Безопасности', 'Тбилиси так же безопасен, как и многие европейские города...', 'metaDescription', 'text', 'safety', 1, 6, 2, NULL, NULL),
(19, 7, NULL, 'Таможня', 'Таможня', 'Вы можете привезти в Грузию 400 сигарет, или 50 сигар, или 50 сигариллов...', 'metaDescription', 'text', 'customs', 1, 7, 2, NULL, NULL),
(20, 8, NULL, 'Связь', 'Связь', 'Сеть Wi-Fi работает в некоторых городских районах. Он бесплатный и не требует никакой регистрации, но довольно медленный...', 'metaDescription', 'text', 'communication', 1, 8, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_texts`
--

CREATE TABLE `post_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_texts_id` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `leftOrRight` enum('right','left') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'right',
  `breadcrumbs` int(11) NOT NULL DEFAULT 1,
  `langID` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_texts`
--

INSERT INTO `post_texts` (`id`, `post_texts_id`, `userID`, `title`, `text`, `leftOrRight`, `breadcrumbs`, `langID`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Basic Facts & Figures', 'Population of Georgia is 4,469,200 , Tbilisi -1,225,000<br>\r\nEthnic composition: Georgians 84%, Azerbaijanis 6.5%, Armenians 5.7%, Russians 1.5%, Ossetians 0.9%, others including Greeks, Ukrainians, Yezids - 1.4%.<br>\r\nTerritory: 69,700 sq km<br>\r\nBorders: Black Sea 330km, The Greater Caucasus Mountain Range forms the 723 km northern border with Russia, Armenia 164km, Azerbaijan 322 km and Turkey 252 km.<br>\r\nLongest river: Mtkvari (Kura) 1515 km<br>\r\nLargest lake: Paravani 37.5 sq km<br>\r\nHighest point: Shkhara (Greater Caucasus ) 5,193 m (17,040 ft)<br>\r\nLocal time: Georgia is in the UTC+4 time zone.<br>\r\nElectricity: Georgian supplies current of 220 volts AC, 50Hz. Sockets require two round pins.', 'left', 1, 1, NULL, NULL),
(2, 2, NULL, 'Visas', 'You do not need a visa to enter and stay in Georgia for up to 360 days if you are a foreign national who permanently resides in the USA, Canada, EU, New Zealand, Israel, United Arab Emirates, Iraq, Japan, Republic of Korea, Brazil, United Mexican States, State of Qatar, Sultanate of Oman, Kingdom of Bahrain, State of Kuwait (check the full list at www.rs.ge/ en/5418) do not need visa to enter and stay in Georgia for up to 360 days. Citizens of the member states of European Union can enter Georgia merely by producing their national identity card. Russian Federation and Ukraine citizens do not need a visa to enter and stay in Georgia for up to 90 days. If you’re not from one of the above countries, you can get a visa from a Georgian embassy or consulate. An “Ordinary” 90-day, single-entry visa, which covers tourism, is US$50. Visas are also issued at the official road and air entry points, but not if you are arriving by rail or sea. Foreign nationals who have a multiple entry US, EU or Schengen member states visa, valid for one year or more and previously used once at least, can enter or stay in Georgia without visa up to 90 days, within the validity term of the visa. Foreign nationals who are holders of the UNO’s or its specialized agencies’ travel documents (Laissez-Passer), can enter or stay on the territory of Georgia without visa up to 90 days.', 'left', 1, 1, NULL, NULL),
(3, 3, NULL, 'Cuisine', 'Georgians are proud of their national cuisine, and rightly so. There are many distinct dishes, with regional variations. Meat, pastries, dumplings, stews and vegetarian dishes are all represented, though fish features only lightly. Georgian food isn’t hot and spicy, but it is certainly flavorsome. Walnuts, pomegranates and garlic, cheese, beans, aborigine, spinach and plums are all used to devastating effect. In the summer, fresh coriander is used extensively, so this could cut down your options if you don’t like it. The soils of Georgia are very rich and the fresh fruit and vegetables they produce are flavor bombs. Sink your teeth into peaches or plums and the taste explodes. Oh, and the grapes…. One of the main reasons Georgian food is so good is because of these amazing ingredients, much of which is produced by small farmers and sold straight to you at the local fruit and veg market. Just buying some bread, tomatoes and fruit here will be a stunning eal. There are many restaurants specializing in Georgian foods in Tbilisi, they vary in quality, of course, but most are good. They wouldn’t last long if they weren’t as Georgians are discerning eaters when it comes to national cuisine. However, if you can, get yourself invited to eat at a Georgian home, that is where the really great food is cooked. You MUST try Georgian food. It is delicious and distinctive.', 'left', 1, 1, NULL, NULL),
(4, 4, NULL, 'Culture', 'Georgian culture is unique and vibrant. Georgians treat their culture very seriously. It is not considered something just for museums and tourists but a vital part of their every day identity. It is not just something valued by older generations but young people too. Georgian culture has evolved over thousands of years and has also absorbed influences from many invaders through the millennia, yet making them their own. The folklore is rich, the dance both beautiful and powerful, the singing is sublime. Literature, fine arts, theatre and music all also have long and strong traditions. If you do nothing else, try and witness some authentic Georgian dancing and listen to a polyphonic choir. You will be moved.', 'left', 1, 1, NULL, NULL),
(5, 5, NULL, 'Arrival & Transport', 'Tbilisi International airport is about 17,5 km East of Tbilisi Centre. The main terminal provides good passenger services. The queues at passport control are usually short and the baggage delivery is prompt. Taxis are just outside the arrival hall and are available 24 hours a day. A taxi ride to the city Centre shouldn’t be more than 20 - 25 Lari. Taxis in Georgia are relatively cheap and it will be the quickest and least stressful option.', 'left', 1, 1, NULL, NULL),
(6, 6, NULL, 'Safety', 'Tbilisi is as safe as many European cities. In essence, the locals are usually friendly and welcoming. Generally, foreigners are seen as a valuable asset and you are unlikely to encounter any problems. If you do have any encounter with crime, the police are friendly and helpful. Police corruption in Georgia is incredibly rare.', 'left', 1, 1, NULL, NULL),
(7, 7, NULL, 'Customs', 'You can bring 400 cigarettes, or 50 cigars or 50 cigarillos or 250 grams of other tobacco products as well as 4 litters of alcoholic beverages into Georgia without declaring it at customs. There are allowances for importing without declaration up to 30kg of food, not exceeding 500 Lari, once in a calendar day. Import or export of money up to 30000 Lari or equivalent in other currencies is exempted from duty. Exporting antiques and old works of arts is subject to licensing and permission from the Ministry of Culture. Cats and dogs must be accompanied by veterinarian health certificate. All international regulations on narcotics, guns and explosives apply.', 'left', 1, 1, NULL, NULL),
(8, 8, NULL, 'Communication', '<b>Internet</b><br><br>\r\n\r\nWiFi network works in some city areas. It is free and doesn’t require any registration, but is rather slow. But almost every café/restaurant/bar has good free WiFi for their customers.<br><br>\r\n\r\n<b>Using a telephone</b><br><br>\r\n\r\nFixed line: Tbilisi telephone code is (+995) 32. To call from a landline within Tbilisi to another number in the city just dial last 7 digits of the telephone number we list. If the number starts with 3 digits (e.g. 559, etc.), dial 0 and then full 9 digits number.<br><br>\r\n\r\n<b>Mobile:</b><br><br>\r\n\r\nYou can get a Georgian SIM-card (sometimes even for free). You’ll need to show your passport. Calling from a mobile to a fixed line phone, dial 0, then the city code (32) then the 7 last digits. Calling to other mobiles is tricky. Sometimes it is enough just to dial the last 9 digits of the number. If that doesn’t work, just dial the full number starting 995. Roaming: To call a fixed line or mobile in Georgia you should dial +995 instead of 00995.', 'left', 1, 1, NULL, NULL),
(9, 9, NULL, 'Business research & Support', 'Accurate information is vital for making successful business and marketing decisions.<br>\r\nWe provide custom-tailored solutions that will meet your research objectives. <br>\r\nOur business advisory and consulting services include:<br><br>\r\n\r\n\r\n• Industry Overviews <br>\r\n• Market Research <br>\r\n• Media Coverage <br>\r\n• Advertising Tracking <br>\r\nConducting of business negotiations.<br><br>\r\n\r\n\r\nUpon your request we prepare high quality reports that deliver the results exactly as you need them.<br>', 'left', 1, 1, NULL, NULL),
(10, 10, NULL, 'Legal Support Services', 'We assist non-Georgian businesses introducing themselves into the local market, support the set-up of incorporation / branch in Georgia, can obtain needed business licenses for foreign-owned companies.<br><br>\r\n\r\n<b>Our services include:</b><br>\r\n\r\n- Incorporation of Subsidiary or Branch Setup in Georgia<br>\r\n- •Corporate Law<br>\r\n- Regulations for Foreign-owned Financial Institutes and Funds<br>\r\n- Business Licenses<br>\r\n- Provision of Director or Representative in Georgia<br>\r\n- Free online advise.<br><br>\r\n\r\nWe could assist you in solving disputes and issues in local courts, providing: <br><br>\r\n \r\nRepresentation in court;<br>\r\nAppealing and discharge of court orders;<br>\r\nAssistance in making and compilation of lawsuits;<br>\r\nRemoval of sequestration;<br>\r\nAssistance in getting compensation for moral and material damage;<br>\r\nDebt recovery under the commercial agreement;<br>\r\nLegal assistance in commercial disputes<br>\r\nAssistance in loan and overdraft arrangements;<br>\r\nRepresentation in banks etc.<br>', 'left', 1, 1, NULL, NULL),
(11, 11, NULL, 'Accounting Advisory Services & Audit', 'As private businessmen and companies continue to seek new developing markets, challenges clearly remain.<br>\r\nYou need to keep an eye on local peculiarities and rules and anticipate the impact of key business decisions on your accounting and financial reporting.<br>\r\nOur Advisory Services practice assists businessmen in accounting and financial reporting challenges and  provide practical, business-focused advice. <br>\r\n<b>Our services include:</b><br>\r\n- General assistance in start-up stage, registration, licensing, banking.<br>\r\n- Every-day accounting services and reporting. Auditing.<br>\r\n- Import – Export support and custom formalities.<br>\r\n- Free online advice.<br>\r\n<b>We supply our customers with:</b><br>\r\n• balance sheet;<br>\r\n• business plan for a loan for the investor or shareholder;<br>\r\n• information on the calculation and optimization of the tax;<br>\r\n• calculations of profitability and cost (product, goods and services)<br>\r\n<b>Audit</b><br>\r\nDuring the comprehensive financial audit, we provide:<br>\r\n\r\n• Complete research of the assets and liabilities of the client’s company, as well as study of costs;<br>\r\n• calculation and comparative analysis of economic performance;<br>\r\n• financial research of funds movement during the accounting period;<br>\r\n• checking the accounting reports, errors detection, correction of inconsistencies in the reporting;<br>', 'left', 1, 1, NULL, NULL),
(12, 12, NULL, 'Real Estate & Accommodation', 'Our company offers you a quality service:<br>\r\n- To find and arrange a sale or lease of property<br>\r\n- To provide market appraisals of properties<br>\r\n- Buy - Sell - Rent of:<br>\r\nLand, private homes, cottages, apartments, construction areas, offices, commercial, administrative, industrial and warehouse areas.<br><br>\r\n\r\n<b>Our areas of expertise also include:</b><br>\r\n• Evaluation, rent review, lease renewal and rating advice<br>\r\n• Property Management<br>\r\n• Development advice for residential, commercial, industrial and leisure schemes<br>\r\n• RICS Registered Values reports and valuations<br><br>', 'left', 1, 1, NULL, NULL),
(13, 13, NULL, 'Accommodation', '<b>Accommodation</b><br>\r\nWhile your being in Georgia we offer you the best price/quality option for your comfort accommodation and enjoyment.', 'left', 1, 1, NULL, NULL),
(14, 1, NULL, 'Основные факты и цифры', 'Население Грузии составляет 4 469 200 человек, Тбилиси - 1 225 000<br>\r\nЭтнический состав: грузины - 84%, азербайджанцы - 6,5%, армяне - 5,7%, россияне - 1,5%, осетины - 0,9%, другие - греки, украинцы, езиды - 1,4%.<br>\r\nТерритория: 69 700 кв. Км<br>\r\nГраницы: Черное море 330 км, Большой Кавказский хребет образует северную границу 723 км с Россией, Арменией 164 км, Азербайджан 322 км и Турцию - 252 км.<br>\r\nСамая длинная река: Мтквари (Кура) 1515 км<br>\r\nСамое большое озеро: Паравани 37,5 кв. Км<br>\r\nСамая высокая точка: Шхара (Большой Кавказ) 5 193 м (17 040 футов)<br>\r\nМестное время: Грузия находится в UTC + 4 часовых пояса.<br>\r\nЭлектричество: грузинский ток питания 220 вольт переменного тока, 50 Гц.<br>', 'left', 1, 2, NULL, NULL),
(15, 2, NULL, 'Визы', 'Вам не нужна виза для въезда и пребывания в Грузии на срок до 360 дней, если вы являетесь иностранным гражданином, который постоянно проживает в США, Канаде, ЕС, Новой Зеландии, Израиле, Объединенных Арабских Эмиратах, Ираке, Японии, Республике Корея , Бразилия, Соединенные Штаты Америки, Государство Катар, Султанат Оман, Королевство Бахрейн, Государство Кувейт (см. Полный список на www.rs.ge/ en/ 5418), не нуждаются в визе для въезда и пребывания в Грузии до 360 дней. Граждане государств-членов Европейского Союза могут въезжать в Грузию только путем составления своего национального удостоверения личности. Граждане Российской Федерации и Украины не нуждаются в визе для въезда и пребывания в Грузии на срок до 90 дней. Если вы не из одной из вышеуказанных стран, вы можете получить визу в посольстве или консульстве Грузии. «Обычная» 90-дневная однократная виза, охватывающая туризм, составляет 50 долларов США. Визы также выдаются на официальных дорожных и воздушных пунктах въезда, но не в том случае, если вы прибываете по железной дороге или в море. Иностранные граждане, имеющие многократную въездную визовую визу из США, ЕС или Шенгена, действительные в течение одного года или более и ранее использовавшиеся по крайней мере один раз, могут въезжать или оставаться в Грузии без визы до 90 дней в течение срока действия визы. Иностранные граждане, являющиеся держателями проездных документов ООН или ее специализированных учреждений (Laissez-Passer), могут въезжать или оставаться на территории Грузии без визы до 90 дней.', 'left', 1, 2, NULL, NULL),
(16, 3, NULL, 'Кухня', 'Грузины гордятся своей национальной кухней, и это правильно. Есть много разных блюд с региональными вариациями. Мясо, выпечка, пельмени, рагу и вегетарианские блюда представлены, хотя рыба в меньшей степени. Грузинская кухня не острая и пряная, но она, безусловно, ароматная. Грецкие орехи, гранаты и чеснок, сыр, бобы, баклажан, шпинат и сливы используются для достожения эффекта. Почвы Грузии очень богаты, а свежие фрукты и овощи, которые они производят, представляют собой ароматические бомбы. Погрузите свои зубы в персики или сливы, и вкус взрывается. О, и виноград .... Одна из главных причин, по которой грузинская еда настолько хороша, - это из-за этих удивительных ингредиентов, большая часть которых производится мелкими фермерами и продается прямо на местном рынке фруктов и овощей. Просто покупка хлеба, помидоров и фруктов здесь будет ошеломляющим. В Тбилиси много ресторанов, специализирующихся на грузинской кухне, они, конечно, различаются по качеству, но большинство из них хорошие. Они не продержались бы долго, если бы они не были такими, так-как грузины взыскательные едоки, когда дело доходит до национальной кухни. Однако, если можно, попробуйте пообедать в грузинском доме, вот где приготовлена &#8203;&#8203;действительно вкусная еда. Вы ДОЛЖНЫ попробовать грузинскую еду. Это вкусно и самобытно.', 'left', 1, 2, NULL, NULL),
(17, 4, NULL, 'культура', 'Грузинская культура уникальна и ярка. Грузины относятся к своей культуре очень серьезно. Это не считается чем-то только для музеев и туристов, но жизненно важной частью их повседневной идентичности. Это не просто то, что ценится старшими поколениями, но и молодыми людьми. Грузинская культура развивалась на протяжении тысяч лет и также ощутила влияние многих захватчиков на протяжении тысячелетий, но сделав их своими. Фольклор богат, танцы прекрасны и сильны, пение возвышенно. Литература, изобразительное искусство, театр и музыка также имеют давние и сильные традиции. Попробуйте посетить некоторые подлинные грузинские танцы и послушайте полифонический хор. Вы будете ошеломлены.', 'left', 1, 2, NULL, NULL),
(18, 5, NULL, 'Прибытие и транспортировка', 'Тбилисский международный аэропорт находится примерно в 17,5 км от центра Тбилиси. Основной терминал обеспечивает хорошие пассажирские услуги. Очереди при паспортном контроле обычно коротки, и доставка багажа осуществляется оперативно. Такси находятся за пределами зала прибытия и доступны 24 часа в сутки. Поездка на такси до центра города не должна превышать 20-30 лари. Такси в Грузии относительно дешевы, и это будет самый быстрый и наиболее выгодный вариант.', 'left', 1, 2, NULL, NULL),
(19, 6, NULL, 'Безопасности', 'Тбилиси так же безопасен, как и многие европейские города. По сути, местные жители обычно дружелюбны и приветливы. Как правило, иностранцы рассматриваются как ценный актив, и вы вряд ли столкнетесь с какими-либо проблемами. Если у вас возникла проблема с преступностью, полиция дружелюбна и полезна. Полицейская коррупция в Грузии невероятно редка.', 'left', 1, 2, NULL, NULL),
(20, 7, NULL, 'Таможня', 'Вы можете привезти в Грузию 400 сигарет, или 50 сигар, или 50 сигариллов, или 250 граммов других табачных изделий, а также 4 литра алкогольных напитков без объявления на таможне. Существуют скидки на импорт без декларации до 30 кг продовольствия, не превышающий 500 лари, один раз в день. Импорт или вывоз денег до 30000 лари или эквивалент в других валютах освобождается от пошлины. Экспорт антиквариата и старых произведений искусства подлежит лицензированию и разрешению Министерства культуры. Кошки и собаки должны сопровождаться ветеринарным сертификатом здоровья. Применяются все международные нормы, касающиеся наркотических средств, оружия и взрывчатых веществ.', 'left', 1, 2, NULL, NULL),
(21, 8, NULL, 'Связь', '<b>интернет</b><br><br>\r\n\r\nСеть Wi-Fi работает в некоторых городских районах. Он бесплатный и не требует никакой регистрации, но довольно медленный. Но почти В каждом кафе / ресторане / баре есть хороший бесплатный WiFi для своих клиентов.<br><br>\r\n\r\n<b>Использование телефона</b><br><br>\r\n\r\nФиксированная линия: телефонный код Тбилиси (+995) 32. Для звонка со стационарного телефона в Тбилиси на другой номер в городе просто наберите последние 7 цифр номера телефона, который мы перечисляем. Если число начинается с 3 цифр (например, 559 и т. Д.), Наберите 0, а затем полный 9-значный номер.<br><br>\r\n\r\n<b>Мобильный телефон:</b><br><br>\r\n\r\nВы можете получить грузинскую SIM-карту (иногда даже бесплатно). Вам нужно будет показать свой паспорт. Вызов с мобильного телефона на стационарный телефон, осуществляется набором 0, затем код города (32), затем 7 последних цифр. Роуминг: для вызова фиксированной линии или мобильного телефона в Грузии вы должны набрать +995 вместо 00995.', 'left', 1, 2, NULL, NULL),
(22, 9, NULL, 'Наши услуги по бизнес-исследованиям', 'Точная информация имеет жизненно важное значение для принятия успешных деловых и маркетинговых решений.<br>\r\nМы предоставляем решения, которые будут отвечать вашим целям.<br>\r\nНаши услуги включают:<br><br>\r\n\r\n• Обзор отрасли<br>\r\n• Исследования рынка<br>\r\n• Освещение в СМИ<br>\r\n• Отслеживание рекламы<br>\r\n• Проведение деловых переговоров.<br><br>\r\n\r\nПо вашему запросу мы готовим отчеты.', 'left', 1, 2, NULL, NULL),
(23, 10, NULL, 'Услуги юридической поддержки', 'Мы помогаем иностранным предприятиям внедряться на местном рынке, регистрацию компании / филиала в Грузии, получить необходимые лицензии для бизнеса.<br>\r\n\r\nНаши услуги:<br>\r\n- Регистрация компании или филиала в Грузии<br>\r\n- Корпоративное право<br>\r\n- Правила для иностранных финансовых институтов и фондов<br>\r\n- Бизнес-лицензии<br>\r\n- Предоставление директора или представителя в Грузии<br>\r\n- Бесплатное онлайн консультирование.<br><br>\r\n\r\nМы можем помочь вам в разрешении споров и вопросов в местных судах, обеспечивая:<br><br>\r\n\r\nПредставительство в суде;<br>\r\nОбжалование и исполнение распоряжений суда;<br>\r\nСодействие в судебных процессах;<br>\r\nЛиквидация и секвестризация;<br>\r\nПомощь в получении компенсации за моральный и материальный ущерб;<br>\r\nВозврат долгов по коммерческому соглашению;<br>\r\nЮридическая помощь в коммерческих спорах<br>\r\nПомощь в кредитовании и овердрафте;<br>\r\nПредставительство в банках и т. Д.<br>', 'left', 1, 2, NULL, NULL),
(24, 11, NULL, 'Бухгалтерские консультационные услуги и аудит', 'Поскольку частные предприниматели и компании продолжают искать новые развивающиеся рынки, проблемы возникают на местах.<br>\r\nВам необходимо знать о местных особенностях и правилах и предвидеть влияние ключевых бизнес-решений на бухгалтерский учет и финансовую отчетность.<br>\r\nНаша практика консультационных услуг помогает предпринимателям в вопросах бухгалтерского учета и финансовой отчетности и предоставляет практические решения.<br>\r\nНаши услуги включают:<br>\r\n- Общая помощь на начальном этапе, регистрация, лицензирование, банковское дело.<br>\r\n- Ежедневные бухгалтерские услуги и отчетность. Аудит.<br>\r\n- Импорт / Экспорт и таможенные формальности.<br>\r\n- Бесплатный онлайн-совет.<br>\r\nМы предостовляем нашим клиентам:<br>\r\n• баланс;<br>\r\n• бизнес-план кредита для инвестора или акционера;<br>\r\n• информацию об исчислении и оптимизации налога;<br>\r\n• расчеты рентабельности и стоимости (продукта, товаров и услуг)<br>\r\n<b>аудит</b><br>\r\nВ ходе комплексного финансового аудита мы предоставляем:<br>\r\n• Полное исследование активов и обязательств компании клиента, а также изучение затрат;<br>\r\n• Расчет и сравнительный анализ экономических показателей;<br>\r\n• Финансовые исследования движения денежных средств за отчетный период;<br>\r\n• Проверка бухгалтерских отчетов, обнаружение ошибок, коррекция несоответствий в отчетности;<br>', 'left', 1, 2, NULL, NULL),
(25, 12, NULL, 'Недвижимость и жилье', 'Наша компания предлагает вам качественный сервис:<br>\r\n- Поиск и организацию продажи или аренды имущества<br>\r\n- Рыночную оценку недвижимости<br>\r\n- Купля - Продажа - Аренда:<br>\r\nЗемли, частных домов, коттеджей, квартир, строительных площадок, офисов, торговых /  административных / промышленных и складских помещений.<br><br>\r\n\r\nТак-же мы делаем:<br>\r\n• Оценку, обзор арендной платы, продление аренды и рекомендации по оценке недвижимости<br>\r\n• Управление недвижимостью<br>\r\n• Консультации по развитию жилых, коммерческих, промышленных и развлекательных систем<br><br>', 'left', 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_texts_images`
--

CREATE TABLE `post_texts_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `imageID` int(11) NOT NULL,
  `image_genderID` int(11) NOT NULL,
  `contentID` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_texts_images`
--

INSERT INTO `post_texts_images` (`id`, `imageID`, `image_genderID`, `contentID`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, NULL, NULL),
(2, 3, 1, 2, 1, NULL, NULL),
(3, 4, 1, 3, 1, NULL, NULL),
(4, 5, 1, 4, 1, NULL, NULL),
(5, 6, 1, 5, 1, NULL, NULL),
(6, 7, 1, 6, 1, NULL, NULL),
(7, 8, 1, 7, 1, NULL, NULL),
(8, 9, 1, 8, 1, NULL, NULL),
(9, 19, 1, 9, 1, NULL, NULL),
(10, 20, 1, 10, 1, NULL, NULL),
(11, 21, 1, 11, 1, NULL, NULL),
(12, 22, 1, 12, 1, NULL, NULL),
(13, 23, 1, 13, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `published_images`
--

CREATE TABLE `published_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `imageID` int(11) NOT NULL,
  `image_genderID` int(11) NOT NULL,
  `contentID` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `defaultOrNot` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `published_images`
--

INSERT INTO `published_images` (`id`, `imageID`, `image_genderID`, `contentID`, `order_id`, `defaultOrNot`, `created_at`, `updated_at`) VALUES
(1, 11, 1, 1, 1, '2', '2019-06-23 20:00:00', NULL),
(2, 12, 1, 2, 1, '2', '2019-06-23 20:00:00', NULL),
(3, 13, 1, 3, 1, '2', '2019-06-23 20:00:00', NULL),
(4, 14, 1, 4, 1, '2', '2019-06-23 20:00:00', NULL),
(5, 15, 1, 5, 1, '2', '2019-06-23 20:00:00', NULL),
(6, 16, 1, 6, 1, '2', '2019-06-23 20:00:00', NULL),
(7, 17, 1, 7, 1, '2', '2019-06-23 20:00:00', NULL),
(8, 18, 1, 8, 1, '2', '2019-06-23 20:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `published_texts`
--

CREATE TABLE `published_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_textID` int(11) NOT NULL,
  `contentID` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `published_texts`
--

INSERT INTO `published_texts` (`id`, `post_textID`, `contentID`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL),
(2, 2, 2, 1, NULL, NULL),
(3, 3, 3, 1, NULL, NULL),
(4, 4, 4, 1, NULL, NULL),
(5, 5, 5, 1, NULL, NULL),
(6, 6, 6, 1, NULL, NULL),
(7, 7, 7, 1, NULL, NULL),
(8, 8, 8, 1, NULL, NULL),
(9, 9, 9, 1, NULL, NULL),
(10, 10, 10, 1, NULL, NULL),
(11, 11, 11, 1, NULL, NULL),
(12, 12, 12, 1, NULL, NULL),
(13, 13, 12, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_images`
--

CREATE TABLE `static_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` int(11) NOT NULL,
  `page` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `static_words`
--

CREATE TABLE `static_words` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` int(11) NOT NULL,
  `page` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `word` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translates`
--

CREATE TABLE `translates` (
  `id` int(10) UNSIGNED NOT NULL,
  `origin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `langID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translates`
--

INSERT INTO `translates` (`id`, `origin`, `translated`, `langID`, `created_at`, `updated_at`) VALUES
(1, 'Planning business here?', 'Планируете визит в Грузию?', 2, NULL, NULL),
(2, 'Briefly inform us of your plans in Georgia.!', 'Кратко сообщите нам о своих планах в Грузии.', 2, NULL, NULL),
(3, 'We offer a range of services to start and support your business in Georgia. If your requirement is not in our list of services, do not hesitate to contact us', 'Хотите получить консультацию от надежных партнеров?\r\nМы предоставляем полный комплекс услуг для удовлетворения потребностей', 2, NULL, NULL),
(4, 'We\'ll make needed arrangements for your enjoyment and success! Just shoot your request or question. Our online advice is free', 'Мы сделаем необходимые приготовления для вашего успеха!\r\nПросто напишите свой запрос или вопрос. Наш онлайн-совет бесплатный!', 2, NULL, NULL),
(5, 'Send', 'Отправить', 2, NULL, NULL),
(6, 'Your name', 'Ваше имя', 2, NULL, NULL),
(7, 'Your E-mail', 'Ваша эл-почта чтобы ответить вам', 2, NULL, NULL),
(8, 'Your Message', 'Ваш отзыв', 2, NULL, NULL),
(9, 'About Us', 'О нас', 2, NULL, NULL),
(10, 'Business research & Support', 'Наши услуги по бизнес-исследованиям', 2, NULL, NULL),
(11, 'Legal support & Business licenses', 'Услуги юридической поддержки', 2, NULL, NULL),
(12, 'Accounting service & Audit', 'Бухгалтерские услуги и аудит', 2, NULL, NULL),
(13, 'Real Estate & Accommodation', 'Недвижимисть и Проживание', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countryID` int(11) DEFAULT NULL,
  `cityID` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userLevel` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_texts`
--
ALTER TABLE `post_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_texts_images`
--
ALTER TABLE `post_texts_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `published_images`
--
ALTER TABLE `published_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `published_texts`
--
ALTER TABLE `published_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_images`
--
ALTER TABLE `static_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_words`
--
ALTER TABLE `static_words`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translates`
--
ALTER TABLE `translates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `post_texts`
--
ALTER TABLE `post_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `post_texts_images`
--
ALTER TABLE `post_texts_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `published_images`
--
ALTER TABLE `published_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `published_texts`
--
ALTER TABLE `published_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `static_images`
--
ALTER TABLE `static_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `static_words`
--
ALTER TABLE `static_words`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translates`
--
ALTER TABLE `translates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
